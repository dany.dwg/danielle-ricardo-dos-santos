<?php

# Iniciando sessão de login 
session_start(); 

# Cabeçalho
include 'header.php';

# Conteúdo da página

	if (isset($_GET['pagina'])) {
		$pagina = $_GET['pagina'];
	}
	else {
		$pagina = 'cursos';
	}
 

switch ($pagina) {
	case 'cursos': include 'views/cursos.php'; break;
	case 'alunos': include 'views/alunos.php'; break;
	case 'matriculas': include 'views/matriculas.php'; break;
	case 'inserir_aluno': include 'views/inserir_aluno.php'; break;
	case 'inserir_curso': include 'views/inserir_curso.php'; break;
	case 'relatorio_aluno_qt': include 'views/relatorio_aluno_qt.php'; break;
	case 'relatorio_aluno_curso': include 'views/relatorio_aluno_curso.php'; break;
	case 'editar_curso': include 'form_editar_curso.php'; break;
	default: include 'views/home.php'; break;
	
}

# Rodapé
include 'footer.php';
