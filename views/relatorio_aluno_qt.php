
<div class="jumbotron animated bounceInLeft">
	<p>Quantidade de alunos por curso</p>
	<table class="table-bordered table-striped table-hover" id="cursos">

		<thead>
			<tr class="table-secondary">
				<th>Código do Curso: &nbsp </th>
				<th>Nome do Curso: &nbsp</th>
				<th>Quantidade de alunos: &nbsp</th>
			</tr>
		</thead>

		<!-- Varrendo e exibindo resultados -->
		<tbody>
			<?php

try{
	$db= new PDO('sqlite:iesb_PDO.sq3');
  
	$query = "select CUR.cod_curso, CUR.nome_curso, COUNT(AL.cod_aluno) AS QT_ALUNO
	from cursos AS CUR
	INNER JOIN alunos AS AL ON CUR.cod_curso = AL.cod_curso_aluno
	GROUP BY CUR.cod_curso, CUR.nome_curso";

  $result = $db->query($query);
  
	foreach($result as $row){ ?>
		<tr>
		<?php
		echo "<td >" .$row['cod_curso']."</td>";
		echo "<td>" .$row['nome_curso']."</td>";
		echo "<td>" .$row['QT_ALUNO']."</td>";?>	
			
		</tr>
		
		
<?php	}
  }catch(PDOException $e){
	echo $e->getMessage();
  }
?>			
		</tbody>
	</table>
</div>