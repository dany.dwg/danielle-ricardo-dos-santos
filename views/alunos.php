
<div class="jumbotron animated bounceInLeft">
	<a class="btn btn-dark" href="?pagina=inserir_aluno">
		Inserir novo Aluno
	</a>
	<table class="table-bordered table-striped table-hover" id="cursos">

		<thead>
			<tr class="table-secondary">
				<th>Código do Aluno: &nbsp </th>
				<th>Nome do Aluno: &nbsp</th>
				<th>Editar &nbsp</th>
				<th>Deletar</th>
			</tr>
		</thead>

		<!-- Varrendo e exibindo resultados -->
		<tbody>
			<?php

try{
	$db= new PDO('sqlite:iesb_PDO.sq3');
  
  $result = $db->query('select * from alunos');
  
	foreach($result as $row){ ?>
		<tr>
		<?php
		echo "<td >" .$row['cod_aluno']."</td>";
		echo "<td>" .$row['nome_aluno']."</td>";?>

		
			<td><a href="form_editar_aluno.php?cod_aluno=<?php echo $row['cod_aluno']; ?>">
						<i class="fas fa-edit"></i>
				</a>
			</td>
			<td><a id="cod_aluno" href="deleta_aluno.php?cod_aluno=<?php echo $row['cod_aluno']; ?>">
						<i class="fas fa-trash-alt" id="cursos-delete"></i>
				</a>
			</td>
		</tr>
		
		
<?php	}
  }catch(PDOException $e){
	echo $e->getMessage();
  }
?>			
		</tbody>
	</table>
</div>