
<div class="jumbotron animated bounceInLeft">
	<a class="btn btn-dark" href="?pagina=inserir_curso">
		Inserir novo curso
	</a>
	<table class="table-bordered table-striped table-hover" id="cursos">

		<thead>
			<tr class="table-secondary">
				<th>Código do Curso: &nbsp </th>
				<th>Nome do Curso: &nbsp</th>
				<th>Editar &nbsp</th>
				<th>Deletar</th>
			</tr>
		</thead>

		<!-- Varrendo e exibindo resultados -->
		<tbody>
			<?php

try{
	$db= new PDO('sqlite:iesb_PDO.sq3');
  
  $result = $db->query('select * from cursos');
  
	foreach($result as $row){ ?>
		<tr>
		<?php
		echo "<td >" .$row['cod_curso']."</td>";
		echo "<td>" .$row['nome_curso']."</td>";?>

		
			<td><a href="form_editar_curso.php?cod_curso=<?php echo $row['cod_curso']; ?>">
						<i class="fas fa-edit"></i>
				</a>
			</td>
			<td><a id="cod_curso" href="deleta_curso.php?cod_curso=<?php echo $row['cod_curso']; ?>">
						<i class="fas fa-trash-alt" id="cursos-delete"></i>
				</a>
			</td>
		</tr>
		
		
<?php	}
  }catch(PDOException $e){
	echo $e->getMessage();
  }
?>			
		</tbody>
	</table>
</div>