<?php if(!isset($_GET['editar'])){ ?>

<h1>Inserir novo Aluno</h1>
<form method="post" action="processa_aluno.php" id="formValidator">
	<br>
	<label class="badge badge-secondary">Nome Aluno:</label><br>
	<input class="form-control" type="text" id="nome_aluno" name="nome_aluno" placeholder="Insira o nome do aluno" >
	<br><br>
	<label class="badge badge-secondary">Endereço:</label><br>
	<input class="form-control" type="text" id="endereco_aluno" name="endereco_aluno" placeholder="Insira o endereço do aluno" >
	<br><br>
	<label class="badge badge-secondary">Curso:</label><br>
	<input type="hidden" name="cod_curso" id="cod_curso" value=""/>
	<select name=cod_curso> 
            <option>Selecione o curso: </option> 
            <?php 
            try{
				$db= new PDO('sqlite:iesb_PDO.sq3');
			  
			  	$result = $db->query('select * from cursos');
			  
				foreach($result as $row){ 				
					echo "<option>". $row['cod_curso']." - ".$row['nome_curso']."</option> ";				
				}			
			}catch(PDOException $e){
				echo $e->getMessage();
			}
			?>	
    </select>       
	<br><br>	
	<input type="submit" class="btn btn-dark" value="Inserir aluno">
</form>
<?php 
} 