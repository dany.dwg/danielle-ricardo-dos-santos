
<div class="jumbotron animated bounceInLeft">
	<p>List de alunos</p>
	<table class="table-bordered table-striped table-hover" id="cursos">

		<thead>
			<tr class="table-secondary">
				<th>Código do aluno: &nbsp </th>
				<th>Nome do aluno: &nbsp</th>
				<th>Curso: &nbsp</th>
			</tr>
		</thead>

		<!-- Varrendo e exibindo resultados -->
		<tbody>
			<?php

try{
	$db= new PDO('sqlite:iesb_PDO.sq3');
  
	$query = "select al.cod_aluno, al.nome_aluno, cur.nome_curso
	from alunos AS al
	INNER JOIN cursos AS cur ON cur.cod_curso = al.cod_curso_aluno
	ORDER BY al.nome_aluno ASC";

  $result = $db->query($query);
  
	foreach($result as $row){ ?>
		<tr>
		<?php
		echo "<td >" .$row['cod_aluno']."</td>";
		echo "<td>" .$row['nome_aluno']."</td>";
		echo "<td>" .$row['nome_curso']."</td>";?>	
			
		</tr>
		
		
<?php	}
  }catch(PDOException $e){
	echo $e->getMessage();
  }
?>			
		</tbody>
	</table>
</div>